package fr.epsi.puissancek.players;

import java.util.ArrayList;
import java.util.List;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;

public class StrongAIPlayer extends Player {

    private List<GameModel> studied = new ArrayList<>();
    
    public StrongAIPlayer(PlayerColor color, GameController controller) {
        super(color, controller);
    }

    @Override
    public void run() {
        play(calculateBestMove());
    }
    
    @Override
    public boolean usesGraphicBoard() {
        return false;
    }
    
    /**
     * Calculate the best possible move for this limited AI.
     * 
     * @return Valid column number
     */
    private int calculateBestMove() {
        studied.clear();
        
        int depth = 6;
        int rating[] = new int[getModel().getWidth()];
        
        for (int i = 0; i < getModel().getWidth(); ++i) {
            GameModel model = new GameModel(getModel());
            if (model.play(i, getColor())) {
                if (model.isGameOver() && model.getWinner() == getColor()) {
                    return i;
                }
                rating[i] = getRating(model, getColor().other(), depth);
            } else {
                rating[i] = -1;
            }
        }
        
        for (int i = 0; i < rating.length; ++i) {
            System.out.println("Colone " + (i + 1) + ": " + rating[i]);
        }
        return getMaxIndex(rating);
    }
    
    private int getRating(GameModel origin, PlayerColor color, int depth) {
        int rating[] = new int[origin.getWidth()];
        for (int i = 0; i < origin.getWidth(); ++i) {
            GameModel model = new GameModel(origin);
            if (model.play(i, color)) {
                if (model.isGameOver()) {
                    if (model.getWinner() == getColor()) {
                        rating[i] = 1;
                    } else {
                        rating[i] = -1;
                    }
                } else {
                    if (depth > 0) {
                        rating[i] = getRating(model, color.other(), depth - 1);
                    } else {
                        rating[i] = 0;
                    }
                }
            } else {
                rating[i] = 0;
            }
        }
        return sum(rating);
    }
    /*
    private int[] getWinningGames(GameModel model, PlayerColor color) {
        int[] columns = new int[model.getWidth()];
        for (int i = 0; i < model.getWidth(); ++i) {
            GameModel m = new GameModel(model);
            if (m.play(i, color)) {
                if (studied.contains(m)) {
                    columns[i] = 0;
                } else {
                    studied.add(m);
                    if (m.isGameOver()) {
                        if (m.getWinner() == getColor()) {
                            columns[i] = 1;
                        } else {
                            columns[i] = 0;
                        }
                    } else {
                        columns[i] = sum(getWinningGames(m, color.other()));
                    }
                }
            } else {
                columns[i] = 0;
            }
        }
        return columns;
    }
    */
    private int sum(int[] tab) {
        int sum = 0;
        for (int i = 0; i < tab.length; ++i) {
            if (tab[i] > 0) {
                sum += tab[i];
            }
        }
        return sum;
    }
    
    private int getMaxIndex(int[] tab) {
        int max = 0;
        int idx = 0;
        for (int i = 0; i < tab.length; ++i) {
            if (tab[i] > max) {
                max = tab[i];
                idx = i;
            }
        }
        return idx;
    }
}
