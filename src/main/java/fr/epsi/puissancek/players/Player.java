package fr.epsi.puissancek.players;

import javax.swing.SwingUtilities;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;
import fr.epsi.puissancek.model.PlayerType;

public abstract class Player {
    
    public static Player build(PlayerColor color, PlayerType type, GameController controller) {
        switch (type) {
        case HUMAN:
            return new HumanPlayer(color, controller);
        case RANDOM_AI:
            return new RandomAIPlayer(color, controller);
        case SIMPLE_AI:
            return new SimpleAIPlayer(color, controller);
        case STRONG_AI:
            return new StrongAIPlayer(color, controller);
        }
        return null;
    }

    private final PlayerColor color;
    private final GameController controller;
    
    /**
     * Create a player with the given color.
     * 
     * @param color Player color
     */
    public Player(PlayerColor color, GameController controller) {
        this.color = color;
        this.controller = controller;
    }
    
    /**
     * Get the player color.
     * 
     * @return Player color
     */
    public PlayerColor getColor() {
        return color;
    }
    
    /**
     * Get the game model.
     * 
     * @return Game model
     */
    public GameModel getModel() {
        return controller.getModel();
    }
    
    /**
     * Tell the controller to play the given move.
     * 
     * @param column Column in which to play
     */
    public void play(int column) {
        SwingUtilities.invokeLater(() -> controller.play(column));
    }
    
    /**
     * Run this player when it's its turn to play.
     */
    public abstract void run();
    
    /**
     * Tell if this player uses the graphic interface.
     * 
     * @return True if and only if this player need the graphic board
     */
    public abstract boolean usesGraphicBoard();
    
}
