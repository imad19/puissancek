package fr.epsi.puissancek.players;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;

public class SimpleAIPlayer extends Player {
    
    public SimpleAIPlayer(PlayerColor color, GameController controller) {
        super(color, controller);
    }

    @Override
    public void run() {
        play(calculateBestMove());
    }
    
    @Override
    public boolean usesGraphicBoard() {
        return false;
    }
    
    /**
     * Calculate the best possible move for this limited AI.
     * 
     * @return Valid column number
     */
    int calculateBestMove() {
        List<Integer> result;
        
        result = getWinningMoves(getModel(), getColor());
        if (! result.isEmpty()) {
            System.out.println("Immediate win!");
            return result.get(0);
        }
        
        result = getWinningMoves(getModel(), getColor().other());
        if (! result.isEmpty()) {
            System.out.println("Prevent immediate win!");
            return result.get(0);
        }
        
        result = getDoubleWinningMoves(getModel(), getColor());
        if (! result.isEmpty()) {
            System.out.println("Double win detected!");
            return result.get(0);
        }
        
        result = getDoubleWinningMoves(getModel(), getColor().other());
        if (! result.isEmpty()) {
            System.out.println("Double win prevented!");
            return result.get(0);
        }
        
        return getDefaultMove();
    }
    
    /**
     * Get a default move (in case there is no winning move found).
     * 
     * @return Default move
     */
    int getDefaultMove() {
        int anyCandidate = -1;
        List<Integer> candidates = IntStream.range(0, getModel().getWidth()).boxed().collect(Collectors.toList());
        ListIterator<Integer> it = candidates.listIterator();
        while (it.hasNext()) {
            int x = it.next();
            
            GameModel model = new GameModel(getModel());
            if (model.play(x, getColor())) {
                anyCandidate = x;
                List<Integer> result = getWinningMoves(model, getColor().other());
                if (result.size() > 0) {
                    it.remove();
                } else {
                    result = getDoubleWinningMoves(model, getColor().other());
                    if (result.size() > 0) {
                        it.remove();
                    }
                }
            } else {
                it.remove();
            }
        }
        
        if (candidates.isEmpty()) {
            return anyCandidate;
        }
        return candidates.get(candidates.size() / 2); // most central position left
    }
    
    /**
     * Get a list of column indexes that lead to immediate victory for the given color.
     * 
     * @param modelStart Start position to analyze
     * @param color Color to play
     * @return List of column indexes for immediate victory
     */
    List<Integer> getWinningMoves(GameModel modelStart, PlayerColor color) {
        List<Integer> wins = new ArrayList<>();
        for (int x = 0; x < getModel().getWidth(); ++x) {
            GameModel model = new GameModel(modelStart);
            model.play(x, color);
            if (model.getWinner() == color) {
                wins.add(x);
            }
        }
        return wins;
    }
    
    /**
     * Get a list of column indexes that lead to double victory for the given color in 2 turns.
     * 
     * @param modelStart Start position to analyze
     * @param color Color to play
     * @return List of column indexes for double victory in 2 turns
     */
    List<Integer> getDoubleWinningMoves(GameModel modelStart, PlayerColor color) {
        List<Integer> doubleWins = new ArrayList<>();
        for (int x = 0; x < getModel().getWidth(); ++x) {
            GameModel model = new GameModel(modelStart);
            if (model.play(x, color)) {
                List<Integer> wins = getWinningMoves(model, color);
                if (wins.size() > 1) {
                    doubleWins.add(x);
                }
            }
        }
        return doubleWins;
    }
}
