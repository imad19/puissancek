package fr.epsi.puissancek.model;

/**
 * Represent a game move (to record the list of moves done).
 */
public class GameMove {

    private PlayerColor player;
    private int column;
    
    /**
     * Create a game move.
     * 
     * @param player Player color
     * @param column Index of the column played
     */
    public GameMove(PlayerColor player, int column) {
        this.player = player;
        this.column = column;
    }
    
    /**
     * Get the player who played this move.
     * 
     * @return Player who played this move
     */
    public PlayerColor getPlayer() {
        return player;
    }
    
    /**
     * Get the column in which this move was played.
     * 
     * @return Column in which this move was played
     */
    public int getColumn() {
        return column;
    }
    
    @Override
    public String toString() {
        return player + " joue en " + (column + 1);
    }
}
