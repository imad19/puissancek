package fr.epsi.puissancek.model;

/**
 * Type of players.
 */
public enum PlayerType {
    
    HUMAN("Humain"),
    RANDOM_AI("Random"),
    SIMPLE_AI("IA simple"),
    STRONG_AI("IA forte");

    private final String description;
    
    private PlayerType(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
        return description;
    }
}
