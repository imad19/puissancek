package fr.epsi.puissancek.model;

/**
 * Possible player colors.
 */
public enum PlayerColor {
    
    RED("Rouge"),
    YELLOW("Jaune");
    
    private final String description;
    
    private PlayerColor(String description) {
        this.description = description;
    }
    
    /**
     * Return the other player's color.
     * 
     * @return Other player's color
     */
    public PlayerColor other() {
        return this == RED ? YELLOW : RED;
    }
    
    @Override
    public String toString() {
        return description;
    }
}
