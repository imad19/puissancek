package fr.epsi.puissancek.model;

import java.util.Arrays;

/**
 * Represent the game board.
 */
public class GameModel {

    private final int width;
    private final int height;
    private final int winningAlignment;
    
    private final PlayerColor grid[];
    
    private PlayerColor winner = null;
    private boolean gameOver = false;
    
    /**
     * Create a game model with size (width, height).
     * 
     * @param width Board width
     * @param height Board height
     * @param winningAlignment Number of aligned tokens required to win
     */
    public GameModel(int width, int height, int winningAlignment) {
        this.width = width;
        this.height = height;
        this.winningAlignment = winningAlignment;
        grid = new PlayerColor[width * height];
    }
    
    /**
     * Create a copy of this game model.
     * 
     * @param gameModel GameModel to copy
     */
    public GameModel(GameModel gameModel) {
        this.winningAlignment = gameModel.winningAlignment;
        this.width = gameModel.width;
        this.height = gameModel.height;
        this.grid = gameModel.grid.clone();
        this.winner = gameModel.winner;
        this.gameOver = gameModel.gameOver;
    }
    
    /**
     * Get the board width in number of cells.
     * 
     * @return Board width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Get the board height in number of cells.
     * 
     * @return Board height
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * Get the number of aligned tokens required to win.
     * 
     * @return Number of aligned tokens required to win
     */
    public int getWinningAlignment() {
        return winningAlignment;
    }
    
    /**
     * Return the number of tokens in the given column.
     * 
     * @return Number of tokens in the given column
     */
    public int getColumnHeight(int x) {
        int h = 0;
        for (int y = 0; y < height; ++y) {
            h += (null != getCell(x, y)) ? 1 : 0; 
        }
        return h;
    }
    
    /**
     * Try to play a token in the given column.
     * If playing is not possible, nothing is done and false is returned.
     * Otherwise, the model is updated and true is returned.
     * 
     * @param x Column in which to play
     * @param player Player color
     * @return True if and only if everything went fine
     */
    public boolean play(int x, PlayerColor player) {
        if (x >= 0 && x < width) {
            int h = getColumnHeight(x);
            if (h < height) {
                updateCell(x, h, player);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Remove the highest token in the given column.
     * If the column is empty, nothing is done.
     * 
     * @param x Column in which to remove a token
     */
    public void remove(int x) {
        if (x >= 0 && x < width) {
            int h = getColumnHeight(x) - 1;
            if (h >= 0) {
                updateCell(x, h, null);
            }
        }
    }
    
    /**
     * Return true if the game is over.
     * The game is over if there is a winner, or if the board is full.
     * 
     * @return True if and only if the game is over
     */
    public boolean isGameOver() {
        return gameOver;
    }
    
    /**
     * Return the color of the winning player (or null if there's no winner).
     * 
     * @return Color of the winning player (or null if there's no winner)
     */
    public PlayerColor getWinner() {
        return winner;
    }
    
    /**
     * Get the value of a cell at position (x, y).
     * If the cell is empty, returns null.
     * 
     * The position (0, 0) represents the bottom-left cell.
     * 
     * @param x X coordinate
     * @param y Y coordinate
     * @return Cell at position (x, y)
     */
    public PlayerColor getCell(int x, int y) {
        return grid[y * width + x];
    }
    
    @Override
    public boolean equals(Object other) {
        if (other instanceof GameModel) {
            GameModel model = (GameModel) other;
            return Arrays.equals(model.grid, grid);
        }
        return false;
    }
    
    /**
     * Update the cell at the given position.
     * Meant for internal use only (use "play" or "remove" methods to add/remove moves).
     * 
     * @param x X coordinate
     * @param y Y coordinate
     * @param color Color
     */
    void updateCell(int x, int y, PlayerColor color) {
        grid[y * width + x] = color;
        
        winner = calculateWinner();
        gameOver = calculateGameOver();
    }
    
    /**
     * Helper method: check if 'WINNING_ALIGNMENT' tokens are vertically aligned up starting from (x, y).
     * 
     * @param x Starting x coordinate
     * @param y Starting y coordinate
     * @return Winning color or null
     */
    PlayerColor checkVertical(int x, int y) {
        PlayerColor firstColor = getCell(x, y);
        for (int k = 1; k < winningAlignment; ++k) {
            if (firstColor != getCell(x, y + k)) {
                return null;
            }
        }
        return firstColor;
    }
    
    /**
     * Helper method: check if 'WINNING_ALIGNMENT' tokens are horizontally aligned right starting from (x, y).
     * 
     * @param x Starting x coordinate
     * @param y Starting y coordinate
     * @return Winning color or null
     */
    PlayerColor checkHorizontal(int x, int y) {
        PlayerColor firstColor = getCell(x, y);
        for (int k = 1; k < winningAlignment; ++k) {
            if (firstColor != getCell(x + k, y)) {
                return null;
            }
        }
        return firstColor;
    }
    
    /**
     * Helper method: check if 'WINNING_ALIGNMENT' tokens are diagonally aligned to top-right starting from (x, y).
     * 
     * @param x Starting x coordinate
     * @param y Starting y coordinate
     * @return Winning color or null
     */
    PlayerColor checkTopRightDiagonal(int x, int y) {
        PlayerColor firstColor = getCell(x, y);
        for (int k = 1; k < winningAlignment; ++k) {
            if (firstColor != getCell(x + k, y + k)) {
                return null;
            }
        }
        return firstColor;
    }
    
    /**
     * Helper method: check if 'WINNING_ALIGNMENT' tokens are diagonally aligned to top-right starting from (x, y).
     * 
     * @param x Starting x coordinate
     * @param y Starting y coordinate
     * @return Winning color or null
     */
    PlayerColor checkTopLeftDiagonal(int x, int y) {
        PlayerColor firstColor = getCell(x, y);
        for (int k = 1; k < winningAlignment; ++k) {
            if (firstColor != getCell(x - k, y + k)) {
                return null;
            }
        }
        return firstColor;
    }
    
    /**
     * Helper method: calculate if the game is over.
     */
    boolean calculateGameOver() {
        if (null != getWinner()) {
            return true;
        }
        
        // if there's no winner, check if board is full
        for (int x = 0; x < width; ++x) {
            if (getColumnHeight(x) < height) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Helper method: calculate if there is a winner with alignments.
     * 
     * @return Color of the winner, or null if there is no winner
     */
    PlayerColor calculateWinner() {
        // vertical
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height - winningAlignment + 1; ++y) {
                PlayerColor winner = checkVertical(x, y);
                if (null != winner) {
                    return winner;
                }
            }
        }
        
        // horizontal
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width - winningAlignment + 1; ++x) {
                PlayerColor winner = checkHorizontal(x, y);
                if (null != winner) {
                    return winner;
                }
            }
        }
        
        // diagonal to top-right
        for (int x = 0; x < width - winningAlignment + 1; ++x) {
            for (int y = 0; y < height - winningAlignment + 1; ++y) {
                PlayerColor winner = checkTopRightDiagonal(x, y);
                if (null != winner) {
                    return winner;
                }
            }
        }
        
        // diagonal to top-left
        for (int x = winningAlignment - 1; x < width; ++x) {
            for (int y = 0; y < height - winningAlignment + 1; ++y) {
                PlayerColor winner = checkTopLeftDiagonal(x, y);
                if (null != winner) {
                    return winner;
                }
            }
        }
        
        return null;
    }
}
