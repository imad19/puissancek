package fr.epsi.puissancek.model;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;

class GameModelTest {

	@Test
	public void test_getColumnHeight() {
		GameModel gm = new GameModel(4,4,2);
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.RED));
		int result = gm.getColumnHeight(1);
		assertEquals(3,result);
	}
	
	@Test
	public void test_getCell() {
		GameModel gm = new GameModel(4,4,2);
		assertTrue(gm.play(0,PlayerColor.YELLOW));
		PlayerColor result = gm.getCell(0,0);
		assertEquals(PlayerColor.YELLOW,result);
	}
	
	@Test
	public void test_remove() {
		GameModel gm = new GameModel(4,4,2);
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.RED));
		gm.remove(1);
		assertEquals(1,gm.getColumnHeight(1));
		assertNull(gm.getCell(gm.getColumnHeight(1),1));
	}
	
	@Test
    void testGetWidth_Height_Align (){
        int r;
        GameModel GameModel = new GameModel(7, 5, 4);
        r = GameModel.getWidth();
        assertNotEquals(0, r);

        r = GameModel.getHeight();
        assertNotEquals(0, r);

        r = GameModel.getWinningAlignment();
        assertNotEquals(0, r);
        
        boolean b = GameModel.isGameOver();
        assertNotNull(b);
    }
	
	@Test
    void test_isWinner (){ 
		GameModel gm = new GameModel(7, 5, 4);
        boolean b = gm.isGameOver();
        assertNotNull(b);
    }
	
	@Test
    void test_play_false (){ 
		GameModel gm = new GameModel(4, 4, 4);
		assertFalse(gm.play(5,PlayerColor.YELLOW));
    }
	
	@Test
    void test_calculateWinner (){ 
        // vertical
		GameModel gm = new GameModel(4, 4, 3);
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertEquals(gm.calculateWinner(),PlayerColor.YELLOW);
		
        // horizontal
		gm = new GameModel(4, 4, 3);
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(2,PlayerColor.YELLOW));
		assertTrue(gm.play(3,PlayerColor.YELLOW));
		assertEquals(gm.calculateWinner(),PlayerColor.YELLOW);
		
        // diagonal to top-right
		gm = new GameModel(4, 4, 3);
		assertTrue(gm.play(0,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.RED));
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertTrue(gm.play(2,PlayerColor.RED));
		assertTrue(gm.play(2,PlayerColor.RED));
		assertTrue(gm.play(2,PlayerColor.YELLOW));
		assertEquals(gm.calculateWinner(),PlayerColor.YELLOW);
		
        // diagonal to top-left
		gm = new GameModel(4, 4, 3);
		assertTrue(gm.play(3,PlayerColor.YELLOW));
		assertTrue(gm.play(2,PlayerColor.RED));
		assertTrue(gm.play(2,PlayerColor.YELLOW));
		assertTrue(gm.play(1,PlayerColor.RED));
		assertTrue(gm.play(1,PlayerColor.RED));
		assertTrue(gm.play(1,PlayerColor.YELLOW));
		assertEquals(gm.calculateWinner(),PlayerColor.YELLOW);
        
    }
	
	

	
	
}
