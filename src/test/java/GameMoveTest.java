import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.GameMove;
import fr.epsi.puissancek.model.PlayerColor;

class GameMoveTest {

	@Test
    void testGetPlayerAndColumn (){
        PlayerColor r;
        int c;
        GameMove gameMove = new GameMove(PlayerColor.RED, 3);
        r = gameMove.getPlayer();
        assertEquals(PlayerColor.RED, r);

        c = gameMove.getColumn();
        assertEquals(3, c);

    }

    @Test
    void testTostring (){
        GameMove gameMove = new GameMove(PlayerColor.RED, 3);
        assertEquals(PlayerColor.RED+ " joue en 4",gameMove.toString());

    }
}
