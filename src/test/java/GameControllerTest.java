import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;
import fr.epsi.puissancek.model.PlayerType;

public class GameControllerTest {

	@Test
    void test_play (){
	        GameModel gameModel = new GameModel(4, 4, 2);

	        GameController gameController = new GameController(gameModel, PlayerType.HUMAN, PlayerType.HUMAN);

	        assertEquals(0, gameController.getMoveList().getSize());

	        PlayerColor Before = gameController.getNextPlayer();

	        gameController.play(1);
	        
	        assertEquals(1, gameController.getMoveList().getSize());

	        assertEquals(Before.other(), gameController.getNextPlayer());
		

    }
	
	void test_unplayable (){
		 GameModel gameModel = new GameModel(4, 4, 2);
	     GameController gameController = new GameController(gameModel, PlayerType.HUMAN, PlayerType.HUMAN);
	     PlayerColor Before = gameController.getNextPlayer();
	     gameController.play(5);
	     assertEquals(0, gameController.getMoveList().getSize());
	     assertNotEquals(Before.other(), gameController.getNextPlayer());
	}
	 
}
