import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.model.PlayerColor;

class PlayerColorTest {

	@Test
	void test_other() {
		PlayerColor pc = PlayerColor.YELLOW;
		PlayerColor result = pc.other();
		assertEquals(PlayerColor.RED,result);
		
	}

}
