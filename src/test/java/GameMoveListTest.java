import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.model.GameMove;
import fr.epsi.puissancek.model.GameMoveList;
import fr.epsi.puissancek.model.PlayerColor;

class GameMoveListTest {

	@Test
	void test_addMove() {
		GameMoveList gml = new GameMoveList();
		GameMove gm = new GameMove(PlayerColor.RED,1);
		assertEquals(0,gml.getSize());
		gml.addMove(gm);
		assertEquals(1,gml.getSize());
		GameMove result =gml.getElementAt(0);
		assertEquals(gm,result);
	}
	
	@Test
	void test_getAndRemoveLastMove() {
		GameMoveList gml = new GameMoveList();
		GameMove gm = new GameMove(PlayerColor.RED,1);
		GameMove gm1 = new GameMove(PlayerColor.YELLOW,1);
		assertEquals(0,gml.getSize());
		gml.addMove(gm);
		gml.addMove(gm1);
		GameMove result =gml.getAndRemoveLastMove();
		assertEquals(1,gml.getSize());
		assertEquals(gm1,result);
	}
	
	@Test
	void test_getElementAt() {
		GameMoveList gml = new GameMoveList();
		GameMove gm = new GameMove(PlayerColor.RED,1);
		GameMove gm1 = new GameMove(PlayerColor.YELLOW,1);
		assertEquals(0,gml.getSize());
		gml.addMove(gm);
		gml.addMove(gm1);
		GameMove result =gml.getElementAt(1);
		assertEquals(gm1,result);
	}

}
